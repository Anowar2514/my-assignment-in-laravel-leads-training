<?php
include_once ('grade_system_class.php');
$subject ="";
$grades ="";
$gradings ="";
if(isset($_POST['submit'])){
    $students = new grade_system_class();
     $bangla = $_POST['bangla'];
     $english = $_POST['english'];
     $math = $_POST['math'];
     $ict = $_POST['ict'];
     $bangladesh = $_POST['bangladesh'];
    $gradings = $students->studentGrade($bangla,$english,$math,$ict,$bangladesh);
    $all_students= new grade_system_class();
    $grades = $all_students->grade($bangla,$english,$math,$ict,$bangladesh);

}
?>
<html>
<head>
    <title>Grading System</title>
    <link type="text/css" rel="stylesheet" href="./css/bootstrap.min.css">
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>

    <div class="panel-body container">
        <h1 class="h1" style="text-align: center; color: #1b6d85"  ><b>Grading System</b></h1>
        <hr />
        <br>

        <form method="post" action=""  >
        <div class="form-group">
            <input type="text" name="bangla" class="form-control" placeholder="Enter your bangla number">
        </div>
            <div class="form-group">
                <input type="text" name="english" class="form-control" placeholder="Enter your english number">
            </div>
            <div class="form-group">
                <input type="text" name="math" class="form-control" placeholder="Enter your math number">
            </div>
            <div class="form-group">
                <input type="text" name="ict" class="form-control" placeholder="Enter your ICT number">
            </div>
            <div class="form-group">
                <input type="text" name="bangladesh" class="form-control" placeholder="Enter your bangladesh number">
            </div>
            <input type="submit" class="btn btn-primary" name="submit" value="Enter All Number">
        </form>
    </div>
    <hr />
       <h3 class="text-center text-success">
           <?php
           echo $subject.'<br>';
           echo $grades.'<br>';
           echo $gradings.'<br>';
           ?>
       </h3>
    <hr />
</body>
</html>
