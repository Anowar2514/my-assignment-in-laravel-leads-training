<?php
/* Check if the key "Volvo" exists in an array: array_key_exists(key,array) */
$a = array('X-Corolla'=>'X90','BMW'=>'X5','Ferrari'=>'W3');
if(array_key_exists('Volvo',$a)){
    echo 'Key is exists'.'<br>';
}
else{
    echo 'Key does not exists'.'<br>';
}

/* Check if the integer key "0" exists in an array: */
$b = array('X-Corolla','BMW','Ferrari');
if(array_key_exists(0,$b)){
    echo 'Key is exists'.'<br>';
}
else{
    echo 'Key does not exists'.'<br>';
}