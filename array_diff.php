<?php
/* Compare the values of two arrays, and return the differences */

$arr01 = array('a'=>'green','b'=>'red','c'=>'blue','d'=>'white');
$arr02 = array('e'=>'green','f'=>'red');
$result = array_diff($arr01,$arr02);
echo'<pre>';
print_r($result);

/* Compare the values of three arrays, and return the differences */

$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
$a2=array("e"=>"red","f"=>"black","g"=>"purple");
$a3=array("a"=>"red","b"=>"black","h"=>"yellow");
$results=array_diff($a1,$a2,$a3);
echo '<pre>';
print_r($results);