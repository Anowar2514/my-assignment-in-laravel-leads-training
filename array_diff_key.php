<?php
/* Compare the keys of two arrays, and return the differences */
$a01 = array('a'=>'red','b'=>'green','c'=>'blue');
$a02 = array('a'=>'red','c'=>'blue','d'=>'pink');
$result = array_diff_key($a01,$a02);
echo '<pre>';
print_r($result);

/* Compare the keys of two indexed arrays, and return the differences */
$a1=array("red","green","blue","yellow");
$a2=array("red","green","blue");

$results=array_diff_key($a1,$a2);
print_r($results);

/* Compare the keys of three arrays, and return the differences */
$a1=array("a"=>"red","b"=>"green","c"=>"blue");
$a2=array("c"=>"yellow","d"=>"black","e"=>"brown");
$a3=array("f"=>"green","c"=>"purple","g"=>"red");

$result_in=array_diff_key($a1,$a2,$a3);
print_r($result_in);

